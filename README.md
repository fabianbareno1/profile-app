
PROFILE APP

This is a Spring boot application.

PRE-REQUISITES:

- It's necessary to have maven installed

In order to get the application up an running you just need to follow this few steps:

1. Clone the repository.
2. Run mvn clean install.
3. Right click in ProfileApplication class an click Run.
4. You will be able to see one of the profiles got from database and consume the exposed endpoints.

Note: This app works in an embedded tomcat, if you want to generate the war of the application,
yous must run  mvn clean package and the war file will be generated in the target folder.

Post data: 8 hours of development, we can improve or include more things of course with more available time.
